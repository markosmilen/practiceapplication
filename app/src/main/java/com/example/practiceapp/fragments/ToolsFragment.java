package com.example.practiceapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.practiceapp.R;

public class ToolsFragment extends Fragment {
    public static final String TAG = ToolsFragment.class.getSimpleName();

    public static ToolsFragment newInstance(){
        ToolsFragment fragment = new ToolsFragment();
        return  fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tools, container, false);
        return view;
    }
}
