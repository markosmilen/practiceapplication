package com.example.practiceapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.practiceapp.R;

public class SummaryFragment extends Fragment {

        public static final String TAG = SummaryFragment.class.getSimpleName();

        public static SummaryFragment newInstance(){
            SummaryFragment fragment = new SummaryFragment();
            return fragment;
        }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_summary, container, false);
        return view;
    }
}
