package com.example.practiceapp.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.practiceapp.fragments.CategoryFragment;
import com.example.practiceapp.fragments.ExerciseFragment;
import com.example.practiceapp.fragments.RoutineFragment;
import com.example.practiceapp.fragments.SummaryFragment;
import com.example.practiceapp.fragments.ToolsFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {

    int numOfItems;

    public PagerAdapter(@NonNull FragmentManager fm, int itemNumber) {
        super(fm);
        this.numOfItems = itemNumber;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return SummaryFragment.newInstance();
            case 1:
                return ExerciseFragment.newInstance();
            case 2:
                return CategoryFragment.newInstance();
            case 3:
                return RoutineFragment.newInstance();
            case 4:
                return ToolsFragment.newInstance();
                default:
                    return null;
        }
    }

    @Override
    public int getCount() {
        return numOfItems;
    }
}
